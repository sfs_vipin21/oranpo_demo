<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        return [
            'id'=> $this->id,
            'name'=> $this->first_name.' '.$this->last_name,
            'username'=> $this->username,
            'email'=> $this->email,
            'phone'=> $this->phone,
            'profile_image'=> $this->profile_image,
            'status'=> $this->status,
        ];
    }
}
